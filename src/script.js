import './style.css'
import * as THREE from 'three'
import { OrbitControls } from 'three/examples/jsm/controls/OrbitControls'

import { DRACOLoader } from 'three/examples/jsm/loaders/DRACOLoader'
import { GLTFLoader } from 'three/examples/jsm/loaders/GLTFLoader'

const canvas = document.querySelector('canvas.webgl')
const canvass = document.querySelector('canvas.webg')
const canvasss = document.querySelector('canvas.web')
const canvassss = document.querySelector('canvas.we')

const sizes = {
    width: window.innerWidth,
    height: window.innerHeight
   
}

const scene = new THREE.Scene()
const scene2 = new THREE.Scene()
const scene3 = new THREE.Scene()
const scene4 = new THREE.Scene()

const camera = new THREE.PerspectiveCamera(80, sizes.width / sizes.height)
const camera2 = new THREE.PerspectiveCamera(70, sizes.width / sizes.height)
const camera3 = new THREE.PerspectiveCamera(40, sizes.width / sizes.height)
const camera4 = new THREE.PerspectiveCamera(100, sizes.width / sizes.height)

camera.position.z = 1
camera.position.x = 15
camera.position.y = 5

camera2.position.z = 1
camera2.position.y = 12
camera2.position.x = 2

camera3.position.z = 10
camera3.position.y = 12
camera3.position.x = 3

camera4.position.z = 1
camera4.position.y = 8
camera4.position.x = 5

scene.add(camera)
scene2.add(camera2)
scene3.add(camera3)
scene4.add(camera4)


const dracoLoader = new DRACOLoader()
dracoLoader.setDecoderPath('draco/')

const gltfLoader = new GLTFLoader()
const gltfLoader2 = new GLTFLoader()
const gltfLoader3 = new GLTFLoader()
const gltfLoader4 = new GLTFLoader()

gltfLoader.setDRACOLoader(dracoLoader)
gltfLoader2.setDRACOLoader(dracoLoader)
gltfLoader3.setDRACOLoader(dracoLoader)
gltfLoader4.setDRACOLoader(dracoLoader)


let ambientLight = new THREE.AmbientLight(new THREE.Color('hsl(0, 0%, 100%)'), 0.75);
scene.add(ambientLight);
let directionalLightBack = new THREE.DirectionalLight(new THREE.Color('hsl(0, 0%, 100%)'), 0.25);
directionalLightBack.position.set(30, 100, 100);
scene.add(directionalLightBack);
let directionalLightFront = new THREE.DirectionalLight(new THREE.Color('hsl(0, 0%, 100%)'), 0.25);
directionalLightFront.position.set(-30, 100, -100);
scene.add(directionalLightFront);



let ambientLight2 = new THREE.AmbientLight(new THREE.Color('hsl(0, 0%, 100%)'), 0.75);
scene2.add(ambientLight2);
let directionalLightBack2 = new THREE.DirectionalLight(new THREE.Color('hsl(0, 0%, 100%)'), 0.25);
directionalLightBack2.position.set(30, 100, 100);
scene2.add(directionalLightBack2);
let directionalLightFront2 = new THREE.DirectionalLight(new THREE.Color('hsl(0, 0%, 100%)'), 0.25);
directionalLightFront2.position.set(-30, 100, -100);
scene2.add(directionalLightFront2);

let ambientLight3 = new THREE.AmbientLight(new THREE.Color('hsl(0, 0%, 100%)'), 0.75);
scene3.add(ambientLight3);
let directionalLightBack3 = new THREE.DirectionalLight(new THREE.Color('hsl(0, 0%, 100%)'), 0.25);
directionalLightBack3.position.set(30, 100, 100);
scene3.add(directionalLightBack3);
let directionalLightFront3 = new THREE.DirectionalLight(new THREE.Color('hsl(0, 0%, 100%)'), 0.25);
directionalLightFront3.position.set(-30, 100, -100);
scene3.add(directionalLightFront3);

let ambientLight4 = new THREE.AmbientLight(new THREE.Color('hsl(0, 0%, 100%)'), 0.75);
scene4.add(ambientLight4);
let directionalLightBack4 = new THREE.DirectionalLight(new THREE.Color('hsl(0, 0%, 100%)'), 0.25);
directionalLightBack4.position.set(30, 100, 100);
scene4.add(directionalLightBack4);
let directionalLightFront4 = new THREE.DirectionalLight(new THREE.Color('hsl(0, 0%, 100%)'), 0.25);
directionalLightFront4.position.set(-30, 100, -100);
scene4.add(directionalLightFront4);

var spotLight = new THREE.SpotLight( 0xFFFFFF,1.0 )
spotLight.position.set(0,0,0)
//spotLight.angle =-1000;
//spotLight.exponent =1;
//spotLight.target.position.set(100,0,0)


var spotLight2 = new THREE.SpotLight( 0xFFFFFF,1.0 )
spotLight2.position.set(0,0,0)

var spotLight3 = new THREE.SpotLight( 0xFFFFFF,1.0 )
spotLight3.position.set(0,0,0)

var spotLight4 = new THREE.SpotLight( 0xFFFFFF,1.0 )
spotLight4.position.set(0,0,0)

scene.add(spotLight)
scene2.add(spotLight2)
scene3.add(spotLight3)
scene4.add(spotLight4)

gltfLoader.load(
    'short.glb',
    (gltf) =>
    {

        scene.add(gltf.scene)

    }
)
gltfLoader2.load(
    'long.glb',
    (gltf) =>
    {

        scene2.add(gltf.scene)

    }
)

gltfLoader3.load(
    't.glb',
    (gltf) =>
    {

        scene3.add(gltf.scene)

    }
)

gltfLoader4.load(
    '2.glb',
    (gltf) =>
    {

        scene4.add(gltf.scene)

    }
)

const cursor ={x:0, y:0}
window.addEventListener('mousemove', (event) =>
{
    cursor.x = event.clientX / sizes.width - 0.5
    cursor.y = -( event.clientY / sizes.width - 0.5)
})


const renderer = new THREE.WebGLRenderer({
    canvas: canvas
})

const renderer2 = new THREE.WebGLRenderer({
    canvas: canvass
})

const renderer3 = new THREE.WebGLRenderer({
    canvas: canvasss
})

const renderer4 = new THREE.WebGLRenderer({
    canvas: canvassss
})

renderer.setClearColor( 0xF2F3F5); 
renderer.setSize(sizes.width , sizes.height)
renderer.setPixelRatio(window.devicePixelRatio);
renderer.shadowMap.enabled = true;
renderer.shadowMap.type = THREE.PCFSoftShadowMap;



renderer2.setClearColor( 0xF2F3F5 ); 
renderer2.setSize(sizes.width , sizes.height)

renderer3.setClearColor( 0xF2F3F5 ); 
renderer3.setSize(sizes.width , sizes.height)

renderer4.setClearColor( 0xF2F3F5 ); 
renderer4.setSize(sizes.width , sizes.height)

const controls = new OrbitControls(camera, canvas)
controls.enableDamping = true;
controls.enablePan = false;
controls.minDistance = 15;
controls.maxDistance = 30;
controls.minPolarAngle = 0.5;
controls.maxPolarAngle = 1.5;
controls.autoRotate = true;
controls.target = new THREE.Vector3(0, 1, 0);
controls.update()


const controls2 = new OrbitControls(camera2, canvass)
controls2.enableDamping = true;
controls2.enablePan = false;
controls2.minDistance = 1;
controls2.maxDistance = 30;
controls2.minPolarAngle = 0.5;
controls2.maxPolarAngle = 1.5;
controls2.autoRotate = true;
//controls2.target = new THREE.Vector3(0, 1, 0);
controls2.update()

const controls3 = new OrbitControls(camera3, canvasss)
controls3.enableDamping = true;
controls3.enablePan = false;
controls3.minDistance = 5;
controls3.maxDistance = 20;
controls3.minPolarAngle = 0.5;
controls3.maxPolarAngle = 1.5;
controls3.autoRotate = true;
controls3.target = new THREE.Vector3(0, 1, 0);
controls3.update()

const controls4 = new OrbitControls(camera4, canvassss)
controls4.enableDamping = true;
controls4.enablePan = false;
controls4.minDistance = 5;
controls4.maxDistance = 20;
controls4.minPolarAngle = 0.5;
controls4.maxPolarAngle = 1.5;
controls4.autoRotate = true;
controls4.target = new THREE.Vector3(0, 1, 0);
controls4.update()

window.addEventListener('dblclick',() =>
{
    if(!document.fullscreenElement)
    {
        canvas.requestFullscreen()
    }
    else
    {
        document.exitFullscreen()
    }
})


window.addEventListener('resize', () => 
{
    sizes.width = window.innerWidth
    sizes.height = window.innerHeight

    camera.aspect = sizes.width / sizes.height
    camera.updateProjectionMatrix()    

    renderer.setSize(sizes.width, sizes.height)
    renderer.setPixelRatio(Math.min(window.devicePixelRatio,2))

    camera2.aspect = sizes.width / sizes.height
    camera2.updateProjectionMatrix()    

    renderer2.setSize(sizes.width, sizes.height)
    renderer2.setPixelRatio(Math.min(window.devicePixelRatio,2))


    camera3.aspect = sizes.width / sizes.height
    camera3.updateProjectionMatrix()    

    renderer3.setSize(sizes.width, sizes.height)
    renderer3.setPixelRatio(Math.min(window.devicePixelRatio,2))

    camera4.aspect = sizes.width / sizes.height
    camera4.updateProjectionMatrix()    

    renderer4.setSize(sizes.width, sizes.height)
    renderer4.setPixelRatio(Math.min(window.devicePixelRatio,2))

})
const animate = () =>
{

    renderer.render(scene, camera)
    controls.update()

    window.requestAnimationFrame(animate)
}
const animate2 = () =>
{

renderer2.render(scene2, camera2)
    controls2.update()

    window.requestAnimationFrame(animate2)
}

const animate3 = () =>
{

    renderer3.render(scene3, camera3)
    controls3.update()

    window.requestAnimationFrame(animate3)
}

const animate4 = () =>
{

    renderer4.render(scene4, camera4)
    controls4.update()

    window.requestAnimationFrame(animate4)
}


animate()
animate2()
animate3()
animate4()